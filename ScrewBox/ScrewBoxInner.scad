// Author: Olivier Lenoir <olivier.len02@gmail.com>
// Created: 2022-11-22 21:02:49
// Updated:
// License: MIT, Copyright (c) 2022 Olivier Lenoir
// Language: OpenSCAD 2021.01
// Project: Screw Box Inner
// Description:


// Special variables
$fa = 3;  // minimum angle
$fs = 0.1;  // minimum size
$fn = undef;  //number of fragments
$t = undef;  // animation step


// Viewport
$vpt = [0, 0, 0];  // translation
$vpr = [50, 0.00, 75];  // rotation angles in degrees
$vpd = 300;  // camera distance
$vpf = undef;  // camera field of view


// include


// use


// Main
cylinder(h=0.8, r=35);
linear_extrude(height=30.0, twist=120)
import("ScrewBox.dxf", layer="inner");

// Modules


// Functions

