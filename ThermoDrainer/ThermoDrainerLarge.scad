// Author: Olivier Lenoir <olivier.len02@gmail.com>
// Language: OpenSCAD 2021.01
// Created: 2023-02-21 17:31:57
// Updated:
// Project: Thermo Drainer
// Description:
// Requested by:


// Special variables
$fs = 1;  // minimum size
$fa = 5;  // minimum angle

// Viewport
//$vpt = [50, 350, 0];  // translation
//$vpr = [50, 0.00, 75];  // rotation angles in degrees
$vpd = 450;  // camera distance

module thermo_grainer_large() {
    difference() {
        intersection() {
            union() {
                for(a_ = [0, 180, 360]) {
            rotate([0, 0, a_])
                linear_extrude(height=130)
                    import(file = "ThermoDrainer.dxf", layer = "linear_extrude");
                }
            }

            rotate_extrude(angle = 360)
                import(file = "ThermoDrainer.dxf", layer = "rotate_extrude");
        }

        union() {
            for(h_ = [20:30:120]) {
                for(a_ = [0:30:360]) {
                    translate([0, 0, h_])
                    rotate([90, 0, a_ + h_ / 2])
                    scale([0.5, 1, 1])
                    cylinder(h = 40, r1 = 0, r2 = 15);
                }
            }
        }
    }
}

render() {
    thermo_grainer_large();
}