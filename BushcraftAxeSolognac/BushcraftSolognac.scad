// Author: Olivier Lenoir <olivier.len02@gmail.com>
// Created: 2023-04-12 18:51:26
// Updated:
// License: MIT, Copyright (c) 2023 Olivier Lenoir
// Language: OpenSCAD 2021.01
// Project: Bushcraft Solognac
// Description:


// Special variables
//$fa = 5;  // minimum angle
//$fs = 0.2;  // minimum size


// Variables
gap = [0.2, 3, 3];
thickness = [3, 3, 3];


// Module
module BushcraftRaw() {
    translate([163.5, 22.7, 113.3])
    rotate([90, 0, 223.5])
    import("BushcraftSolognac5000.stl");
}


module Bushcraft() {
    union() {
        BushcraftRaw();
        mirror([1, 0, 0]) BushcraftRaw();
    }
}


module Form() {
    difference() {
        union() {
            hull()
            intersection() {
                Bushcraft();
                translate([0, 30, 43])
                cylinder(h=100, d=50);
            }

            hull()
            difference() {
                Bushcraft();
                translate([0, 30, 43])
                cylinder(h=100, d=50);
            }
        }
        hull() {
            translate([0, 66, 62])
            rotate([0, 90, 0])
            cylinder(h=30, d=23, center=true);

            translate([0, 77.5, 90])
            rotate([0, 90, 0])
            cylinder(h=30, d=23, center=true);
        }
    }
}


module FormPlus() {
    minkowski() {
        Form();
        resize(gap)
        sphere(10);
    }
}


module Holder() {
    minkowski() {
        Form();
        resize(gap + 2 * thickness)
        sphere(10);
    }
}


module Clip() {
    rotate([90, 0, 0])
    linear_extrude(40)
    import("Clip.dxf");
}


module BushcraftHolder() {
    difference() {
        union() {
            Holder();
            translate([10, 54.5, 5])
            Clip();
        }
        FormPlus();
        union() {
            cube([35, 300, 10], center=true);

            translate([0, 0, 160])
            rotate([-64, 0, 0])
            cube([30, 200, 200], center=true);

            translate([-20, 17.3, 0])
            cube([20, 37, 100]);
        }
    }
}


// Main
BushcraftHolder();
