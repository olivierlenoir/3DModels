// Author: Olivier Lenoir <olivier.len02@gmail.com>
// Created: 2023-03-04 13:04:43
// Updated:
// License: MIT, Copyright (c) 2023 Olivier Lenoir
// Language: OpenSCAD 2021.01
// Project: T junction
// Description: Garden Feelings 5190


// Special variables
$fa = 5;  // minimum angle
$fs = 0.1;  // minimum size
$fn = undef;  //number of fragments
$t = undef;  // animation step


// Viewport
//$vpt = [0, 0, 30];  // translation
//$vpr = [50, 0.00, 75];  // rotation angles in degrees
//$vpd = 350;  // camera distance
//$vpf = undef;  // camera field of view


// include


// use


// Functions


// Modules
module T_Junction() {
    intersection() {
        difference() {
            union() {
                cylinder(h=55, d=25);
                rotate([90, 0, 0]) {
                    cylinder(h=110, d=25, center=true);
                }
                translate([0, 0, -6.25]) {
                    cube([12.5, 110, 12.5], center=true);
                }
                scale([1/8, 1, 1]) {
                    sphere(d=70);
                }
            }
            union() {
                cylinder(h=55, d=19.6);
                rotate([90, 0, 0])
                cylinder(h=110, d=19.6, center=true);
            }
        }
    translate([-12.5, -55, -12.5])
        cube([25, 110, 67.5]);
    }

}


// Main
render()
    T_Junction();
