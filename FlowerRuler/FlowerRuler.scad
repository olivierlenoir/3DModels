// Author: Olivier Lenoir <olivier.len02@gmail.com>
// Created:
// Updated:
// License: MIT, Copyright (c) <YYYY> Olivier Lenoir
// Language: OpenSCAD 2021.01
// Project:
// Description:


// Special variables
//$fa = 5;  // minimum angle
//$fs = 0.1;  // minimum size
//$fn = undef;  //number of fragments
//$t = undef;  // animation step


// Viewport
//$vpt = [0, 0, 0];  // translation
//$vpr = [50, 0.00, 75];  // rotation angles in degrees
//$vpd = 600;  // camera distance
//$vpf = undef;  // camera field of view


// include


// use


// Functions


// Modules
module Ruler() {
    hull() {
        for(y=[-130, 130]) {
            translate([0, y, 0])
            cylinder(h=3, d=15, center=true);
        }
    }
}


module Hole() {
    union() {
        //cylinder(h=3, d=10, center=true);
        cylinder(h=3, d=4, center=true);
        cylinder(h=3, d1=0, d2=8, center=true);
    }
}


module Groove() {
    hull() {
        for(y=[-50, 50]) {
            translate([0, y, 0])
            cylinder(h=1, d=9);
        }
    }
}


module FlowerRuler() {
    difference() {
        //cube([15, 300, 3], center=true);
        Ruler();
        
        for(y=[-130, 0, 130]) {
            translate([0, y, 0])
            Hole();
        }
        
        for(y=[-65, 65]) {
            translate([0, y, 0.5])
            Groove();
        }
    }
}


// Main
//Hole();
//Groove();
render()
rotate([0, 0, 45])
FlowerRuler();