// Author: Olivier Lenoir <olivier.len02@gmail.com>
// Created: 2023-11-04 16:10:48
// Updated:
// License: MIT, Copyright (c) 2023 Olivier Lenoir
// Language: OpenSCAD 2021.01
// Project: Jute Quiver
// Description:


fn_low = 16;
fn_high = 240;

$fn = fn_low;


// Variables
quiver_width = 140;
quiver_height = 200;
quiver_border_height = 50;
quiver_rod_diameter = 4;
broadhead_border = 27;
thickness = 3;
gap = 0.4;

quiver_diameter = 2 * quiver_width / PI;
quiver_cone_height = ((quiver_height - quiver_border_height) ^ 2 - (quiver_diameter / 2) ^ 2) ^ 0.5;


// include


// use


// Functions


// Modules
module broadhead_slice(fn) {
    tickness_0 = 2;
    translate([0, 0, 11.5])
    union() {
        rotate([90, 0, 90])
        union() {
            translate([0, 0, -tickness_0 / 2])
            linear_extrude(tickness_0, $fn=fn)
            import("MagnusClassicBroadhead.dxf", layer="CaseSlice");
        }

        rotate_extrude($fn=fn)
        import("MagnusClassicBroadhead.dxf", layer="CaseFullTaper");
    }
}


module broadhead_case() {
    difference() {
        minkowski() {
            broadhead_slice(fn_low);
            sphere(thickness, $fn=fn_low);
        }
        translate([0, 0, -thickness])
        cylinder(h=thickness, r=25);
        //broadhead_slice(fn_high);
    }
}


module broadhead_quiver_case() {
    difference() {
        union() {
            // disc
            linear_extrude(height=thickness, $fn=fn_high)
            circle(d=quiver_diameter - 2 * (thickness + gap));

            linear_extrude(height=broadhead_border, $fn=fn_high)
            difference() {
                circle(d=quiver_diameter - 2 * (thickness + gap));
                circle(d=quiver_diameter - 4 * thickness - 2 * gap);
            }

            // broadhead_case
            for(a=[0:360/6:360]) {
                rotate([0, 0, a])
                translate([10, 15, 0])
                broadhead_case();
            }
        }

        // quiver rod
        x_ = (quiver_diameter - quiver_rod_diameter) / 2 - thickness;
        for(x=[-x_, x_]) {
            translate([x, 0, 0])
            cylinder(h=quiver_border_height, d=quiver_rod_diameter + 2 * (thickness + gap), $fn=fn_high);
        }

        // broadhead_slice
        for(a=[0:360/6:360]) {
            rotate([0, 0, a])
            translate([10, 15, 0])
            broadhead_slice(fn_high);
        }
    }
}


module outer_quiver_case() {
    difference() {
        union() {
            // quiver border
            linear_extrude(height=quiver_border_height, $fn=fn_high)
            difference() {
                circle(d=quiver_diameter);
                circle(d=quiver_diameter - 2 * thickness);
            }

            // quiver rod
            x_ = (quiver_diameter - quiver_rod_diameter) / 2 - thickness;
            for(x=[-x_, x_]) {
                translate([x, 0, 0])
                difference() {
                    union() {
                        cylinder(h=quiver_border_height, d=quiver_rod_diameter + 2 * thickness, $fn=fn_high);
                        translate([0, 0, quiver_border_height])
                        sphere(d=quiver_rod_diameter + 2 * thickness, $fn=fn_high);
                    }
                    cylinder(h=quiver_border_height, d=quiver_rod_diameter, $fn=fn_high);
                }
            }

            // quiver border tore
            translate([0, 0, quiver_border_height])
            rotate_extrude($fn=fn_high)
            translate([quiver_diameter / 2 - thickness, 0 , 0])
            circle(r=thickness);

            // bottom quiver
            difference() {
                minkowski() {
                    translate([0, 0, quiver_border_height])
                    linear_extrude(
                        height=quiver_cone_height,
                        scale=[PI / 2, 0]
                    )
                    circle(d=quiver_diameter - 4 * thickness);
                    sphere(2 * thickness);
                }

                translate([0, 0, quiver_border_height - 2 * thickness])
                cylinder(h=2 * thickness, d=quiver_diameter);

                translate([0, 0, quiver_border_height])
                linear_extrude(
                    height=quiver_cone_height,
                    scale=[PI / 2, 0]
                )
                circle(d=quiver_diameter - 2 * thickness);
            }
        }
    }
}


// Main
//outer_quiver_case();
//broadhead_slice(fn_high);
//broadhead_case();
broadhead_quiver_case();

if(0) {
    intersection() {
        outer_quiver_case();
        translate([0, 0, quiver_border_height - thickness - broadhead_border])
        broadhead_quiver_case();
    }
}
