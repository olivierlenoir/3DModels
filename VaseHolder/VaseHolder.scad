// Author: Olivier Lenoir <olivier.len02@gmail.com>
// Created: 2023-04-10 09:44:05
// Updated:
// License: MIT, Copyright (c) 2023 Olivier Lenoir
// Language: OpenSCAD 2021.01
// Project: Vase Holder
// Description:


// Modules
module Pin() {
    translate([0, 0, -3])
    union() {
        cylinder(h=15, d=15);
        translate([0, 0, 15])
        sphere(d=15);
    }
}

module VaseHolder() {
    difference() {
        union() {
            rotate_extrude(angle=360)
            import("VaseHolder.dxf", layer="VaseHolder");
            Pin();
        }
        for(i = [0 : 360/7 : 360]) {
            rotate([0, 0, i])
            translate([48, 0, 0])
                cylinder(d=30, h=200, center=true);
            rotate([0, 0, i + 180])
            translate([20, 0, 0])
                cylinder(d=15, h=200, center=true);
            translate([0, 0, 50])
            rotate([90, 0, i])
                cylinder(d=25, h=200, center=true);
        }
        for(i = [0 : 360/14 : 360]) {
            rotate([0, 0, i + 90])
            translate([75, 0, 0])
                cylinder(d=20, h=200, center=true);
        }
        translate([0, 0, 250])
            cube(400, center=true);
        cylinder(d=3, h=200, center=true);
    }
}


module VaseGauge() {
    rotate([90, 0, 0])
    intersection() {
        rotate_extrude(angle=360)
            import("VaseHolder.dxf", layer="VaseHolder");
        cube([400, 3, 400], center=true);
    }
}


// Main
VaseHolder();
