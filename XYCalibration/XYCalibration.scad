// Author: Olivier Lenoir <olivier.len02@gmail.com>
// Created: 2022-11-06 12:36:40
// Updated:
// License: MIT, Copyright (c) 2022 Olivier Lenoir
// Language: OpenSCAD 2021.01
// Project: X Y calibration 140mm x 140mm x 5mm
// Description:


// Special variables
$fa = 5;  // minimum angle
$fs = 0.1;  // minimum size
$fn = undef;  //number of fragments
$t = undef;  // animation step


// Viewport
$vpt = [45, 100, 10];  // translation
$vpr = [50, 0.00, 75];  // rotation angles in degrees
$vpd = 565;  // camera distance
$vpf = undef;  // camera field of view


// include


// use


// Main
difference() {
    cube([140, 140, 5]);
    translate([0.8, 0.8, -0.8])
        cube([140 - 1.6, 140 - 1.6, 10]);
}

// Modules


// Functions

