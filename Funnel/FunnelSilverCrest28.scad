// Author: Olivier Lenoir <olivier.len02@gmail.com>
// Created: 2022-12-18 14:46:32
// Updated:
// License: MIT, Copyright (c) 2022 Olivier Lenoir
// Language: OpenSCAD 2021.01
// Project: Funnel for SilverCrest bag 28cm
// Description:


// Special variables
$fa = 5;  // minimum angle
$fs = 1;  // minimum size
$fn = undef;  //number of fragments
$t = undef;  // animation step


// Viewport
$vpt = [0, 0, 70];  // translation
$vpr = [50, 0.00, 75];  // rotation angles in degrees
$vpd = 700;  // camera distance
$vpf = undef;  // camera field of view


// Mddule
module Funnel(h_, length_M, length_m, th) {
    d_=(length_M*2)/PI;
    difference() {
        linear_extrude(height=h_, scale=[length_m/d_, 2*th/d_])
            circle(d=d_);
        linear_extrude(height=h_+th, scale=[length_m/d_, 2*th/d_])
            circle(d=d_-2*th);
    }
}


// Main
Funnel(140, 280, 210, 3);