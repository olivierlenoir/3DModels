// Author: Olivier Lenoir <olivier.len02@gmail.com>
// Created: 2024-12-14 17:53
// Updated:
// License: MIT, Copyright (c) 2024 Olivier Lenoir
// Language: OpenSCAD 2021.01
// Project: NBA6 Stand
// Description:


// Special variables
//$fa = 5;  // minimum angle
//$fs = 0.1;  // minimum size
//$fn = undef;  //number of fragments
//$t = undef;  // animation step


// Viewport
//$vpt = [45, 100, 10];  // translation
//$vpr = [50, 0.00, 75];  // rotation angles in degrees
//$vpd = 565;  // camera distance
//$vpf = undef;  // camera field of view


// Variables
nba6_x = 127 - 10;
nba6_y = 127.5 - 10;
h_1 = 15;
h_2 = 35;
h_3 = 5;


// Modules
module nba6_base(base_x=nba6_x, base_y=nba6_y) {
    difference() {
        offset(r=5) {
            square([base_x, base_y], center=true);
        }
        offset(r=2) {
            square([base_x, base_y], center=true);
        }
    }
}


module nba6_stand() {
    linear_extrude(height=h_1) {
        nba6_base();
    }
    translate([0, 0, h_1])
    linear_extrude(height=h_2, twist=30, slices=60) {
        nba6_base();
    }
    translate([0, 0, 50])
    rotate([0, 0, -30])
    linear_extrude(height=h_3) {
        nba6_base();
    }   
}


module nba6_gage() {
    linear_extrude(height=2) {
        nba6_base();
    }
}


module nba6_holes() {
    translate([0, 0, 12])
    union() {
        for(t_x=[-50:20:50]) {
            translate([t_x, 0, 0])
            rotate([90, 0, 0])
            cylinder(h=250, r=8, center=true);
        }
        for(t_x=[-30:20:30]) {
            translate([t_x, 0, 30])
            rotate([90, 0, 0])
            cylinder(h=250, r=8, center=true);
        }
        for(t_x=[-40:20:20]) {
            translate([t_x, 0, 15])
            rotate([90, 0, 0])
            cylinder(h=250, r=8, center=true);
        }
    }
}


// Main
//nba6_gage();

difference() {
    nba6_stand();
    nba6_holes();
    rotate([0, 0, 90])
    nba6_holes();
    cube([250, 75, 30], center=true);
}