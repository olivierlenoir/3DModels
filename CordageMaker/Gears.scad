// Author: Olivier Lenoir <olivier.len02@gmail.com>
// Created: 2023-04-22 18:39:52
// Updated:
// License: MIT, Copyright (c) 2023 Olivier Lenoir
// Language: OpenSCAD 2021.01
// Project: Cordage Maker
// Description:


// Special variables
$fa = 5;  // minimum angle
$fs = 0.2;  // minimum size


// include


// use
use <MCAD/involute_gears.scad>


// Functions


// Modules
module Torus(r=1.5+0.15, hub_diameter=15) {
    rotate_extrude(convexity = 10)
    hull() {
        translate([hub_diameter/2, r, 0])
        circle(r=r);
        translate([hub_diameter/2, -r, 0])
        circle(r=r);
    }
}


module MyGear(teeth=17, circles=8, circular_pitch=500) {
    height=15;
    twist=10*height;
    pressure_angle=30;
        difference() {
            gear(number_of_teeth=teeth,
                circular_pitch=circular_pitch,
                pressure_angle=pressure_angle,
                clearance = 0.2,
                gear_thickness = height/2,
                rim_thickness = height/2,
                rim_width = 5,
                hub_thickness = height/2,
                hub_diameter=15,
                bore_diameter=5,
                circles=circles,
                twist=twist/teeth);
            translate([0,0,height/2])
            Torus();
        }
    mirror([0,0,1]) {
        difference() {
            gear(number_of_teeth=teeth,
                circular_pitch=circular_pitch,
                pressure_angle=pressure_angle,
                clearance = 0.2,
                gear_thickness = height/2,
                rim_thickness = height/2,
                rim_width = 5,
                hub_thickness = height/2,
                hub_diameter=15,
                bore_diameter=5,
                circles=circles,
                twist=twist/teeth);
            translate([0,0,height/2])
            Torus();
        }
    }
}


module MyGears() {
    translate([60, 0, 0])
    difference() {
        MyGear(teeth=15, circles=7);
        cylinder(d=6.3, h=20, center=true, $fn=6);
    }
    mirror([0,1,0])
    difference() {
        MyGear(teeth=23, circles=7);
        cylinder(d=8.3, h=20, center=true, $fn=6);
    }
}


// Main
//projection(cut=true)
MyGears();
