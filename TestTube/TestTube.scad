// Author: Olivier Lenoir <olivier.len02@gmail.com>
// Created: 2022-11-20 14:44:09
// Updated:
// License: MIT, Copyright (c) 2022 Olivier Lenoir
// Language: OpenSCAD 2021.01
// Project: Test Tube
// Description:


// Special variables
$fa = 5;  // minimum angle
$fs = 0.1;  // minimum size
$fn = undef;  //number of fragments
$t = undef;  // animation step


// Viewport
$vpt = [45, 100, 10];  // translation
$vpr = [50, 0.00, 75];  // rotation angles in degrees
$vpd = 565;  // camera distance
$vpf = undef;  // camera field of view


// include


// use


// Main
cylinder(h=1.5, d=30);
translate([0, 0, 1.5])
cylinder(h=1.5, d1=30, d2=25);
difference() {
    cylinder(h=100, d=25);
    translate([0, 0, 3])
    cylinder(h=100, d=25-2*0.8);
}


// Modules


// Functions

