// Author: Olivier Lenoir <olivier.len02@gmail.com>
// Created: 2022-11-13 15:29:45
// Updated:
// License: MIT, Copyright (c) 2022 Olivier Lenoir
// Language: OpenSCAD 2021.01
// Project: Magnus classic broadhead quiver
// Description:


// Special variables
$fa = 5;  // minimum angle
$fs = 0.1;  // minimum size
$fn = undef;  //number of fragments
$t = undef;  // animation step


// Viewport
$vpt = [0, 0, 30];  // translation
$vpr = [50, 0.00, 75];  // rotation angles in degrees
$vpd = 200;  // camera distance
$vpf = undef;  // camera field of view


// include


// use


// Modules
module BroadheadCase() {
    include <MagnusClassicBroadheadCase.scad>
}

// Functions


// Main
difference() {
    union() {
        BroadheadCase();
        translate([7.6, 24, 0])
        BroadheadCase();
    }
    translate([3.8, 12, 32])
    cube([4, 9, 40], center=true);
}