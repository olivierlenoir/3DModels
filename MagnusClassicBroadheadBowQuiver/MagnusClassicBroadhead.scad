// Author: Olivier Lenoir <olivier.len02@gmail.com>
// Created: 2022-11-12 18:39:04
// Updated:
// License: MIT, Copyright (c) 2022 Olivier Lenoir
// Language: OpenSCAD 2021.01
// Project:  Magnus Classic Broadhead
// Description:


// Special variables
$fa = 5;  // minimum angle
$fs = 0.1;  // minimum size
$fn = undef;  //number of fragments
$t = undef;  // animation step


// Viewport
$vpt = [0, 0, 30];  // translation
$vpr = [75, 0.00, 70];  // rotation angles in degrees
$vpd = 180;  // camera distance
$vpf = undef;  // camera field of view


// include


// use


// Main
rotate([90, 0, 90])
union() {
    tickness_0 = 0.7;
    color("gray")
    translate([0, 0, -tickness_0 / 2])
    linear_extrude(tickness_0)
    import("MagnusClassicBroadhead.dxf", layer="0");

    tickness_1 = 1.4;
    color("darkgray")
    translate([0, 0, -tickness_1 / 2])
    linear_extrude(tickness_1)
    import("MagnusClassicBroadhead.dxf", layer="1");
}

color("darkgray")
rotate_extrude()
import("MagnusClassicBroadhead.dxf", layer="Taper");


// Modules


// Functions

