// Author: Olivier Lenoir <olivier.len02@gmail.com>
// Created: 2022-11-12 22:27:32
// Updated:
// License: MIT, Copyright (c) 2022 Olivier Lenoir
// Language: OpenSCAD 2021.01
// Project: Magnus Classic Broadhead Case
// Description:


// Special variables
$fa = 5;  // minimum angle
$fs = 0.1;  // minimum size
$fn = undef;  //number of fragments
$t = undef;  // animation step


// Viewport
$vpt = [0, 0, 30];  // translation
$vpr = [50, 0.00, 75];  // rotation angles in degrees
$vpd = 200;  // camera distance
$vpf = undef;  // camera field of view


// include


// variable
tickness_0 = 2;


// Modules
module Broadhead() {
    include <MagnusClassicBroadhead.scad>
}


module BroadheadCase() {
    rotate([90, 0, 90])
    union() {
        translate([0, 0, -tickness_0 / 2])
        linear_extrude(tickness_0)
        import("MagnusClassicBroadhead.dxf", layer="CaseBlade");
    }

    rotate_extrude()
    import("MagnusClassicBroadhead.dxf", layer="CaseFullTaper");
}


module BroadheadSlice() {
    rotate([90, 0, 90])
    union() {
        translate([0, 0, -tickness_0 / 2])
        linear_extrude(tickness_0)
        import("MagnusClassicBroadhead.dxf", layer="CaseSlice");
    }

    rotate_extrude()
    import("MagnusClassicBroadhead.dxf", layer="CaseFullTaper");
}


// Functions


// Main
rotate([0, 0, 10])
difference() {
    //%translate([0, 0, 35])
    //cube([16, 40, 70], center=true);
    rotate([0, 0, -5])
    union() {
        resize([4, 8, 0], [true, true, false])
        translate([0, 0, 63])
        sphere(d=13);
        resize([16, 0, 0], [true, false, false])
        rotate_extrude()
        import("MagnusClassicBroadhead.dxf", layer="Case");
    }

    union() {
        BroadheadSlice();
        for(a= [0:-2:-10]) {
            rotate([0, 0, a])
            BroadheadCase();
        }
        // Magnet
        d_blade = 0.8;
        for(a=[0, 180]) {
            rotate([0, 0, -10 + a])
            translate([tickness_0 / 2 + d_blade, 12, 12])
            rotate([0, 90, 0])
            cylinder(h=10, d=9);
        }
    }
}