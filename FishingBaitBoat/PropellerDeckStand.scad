// Author: Olivier Lenoir <olivier.len02@gmail.com>
// Created: 2022-12-11 14:11:23
// Updated:
// License: MIT, Copyright (c) 2022> Olivier Lenoir
// Language: OpenSCAD 2021.01
// Project: Propeller Deck Stand
// Description:


// Special variables
$fa = 3;  // minimum angle
$fs = 0.1;  // minimum size
$fn = undef;  //number of fragments
$t = undef;  // animation step


// Viewport
$vpt = [0, 0, 0];  // translation
$vpr = [50, 0.00, 75];  // rotation angles in degrees
$vpd = 600;  // camera distance
$vpf = undef;  // camera field of view


// include
include <FishingBaitBoat.scad>

// use


// Main
rotate([0, -90, 90])
PropellerDeckStand();

// Modules


// Functions

