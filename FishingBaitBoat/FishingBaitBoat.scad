// Author: Olivier Lenoir <olivier.len02@gmail.com>
// Created: 2022-11-26 12:30:27
// Updated:
// License: MIT, Copyright (c) 2022 Olivier Lenoir
// Language: OpenSCAD 2021.01
// Project: Fishing Bait Boart
// Description:


// Modules
module MainDeckProfile(deck_length) {
    translate([0, -deck_length/2, 0])
    rotate([-90, 0, 0])
    linear_extrude(height=deck_length, convexity=10)
    import("FishingBaitBoatDeck.dxf", layer="MainDeck");
}


module SternDeckProfile(deck_length) {
    translate([0, -deck_length/2, 0])
    rotate([-90, 0, 0])
    linear_extrude(height=deck_length, convexity=10)
    import("FishingBaitBoatDeck.dxf", layer="SternDeck");
}


module DeckHole(hole_d, space, n, m) {
    i = 100;
    for(i=[-n:n]) {
        for(j=[-m:m]) {
            translate([i*space, j*space, 0])
            cylinder(h=240, d=hole_d, center=true);
        }
    }
}


module SideProfile(tickness, length, n) {
    m = n - 1;
    space = (length - 2*tickness) / m;
    translate([0, -m*space/2, 0]) 
    {
        for(i=[0:m]) {
            translate([0, i*space, 0]) {
                // deck
                translate([0, 0, tickness/2])
                cube([10*length, 10*length, tickness], center=true);
                // beem
                //translate([0, 0, 0])
                cube([10*length, 2*tickness, 10*length], center=true);
            }
        }
    }
}


module BrushlessMotorHolder() {
    linear_extrude(height=10, convexity=10)
    import("BrushlessPropellerHolder.dxf", layer="BrushlessMotorHolder");
}


module BearingHolder() {
    translate([0, 0, 10])
    linear_extrude(height=110, convexity=10)
    import("BrushlessPropellerHolder.dxf", layer="BearingHolder");
}


module HolderProfile() {
    translate([0, 50, 0])
    rotate([90, 0, 0])
    linear_extrude(height=100, convexity=10)
    import("BrushlessPropellerHolder.dxf", layer="HolderProfile");
}


module PropellerDeckStand() {
    difference() {
        union() {
            rotate([90, 0, 0])
            linear_extrude(height=70, convexity=10, center=true)
            import("PropellerDeckStand.dxf", layer="PropellerDeckStand");

            rotate([0, 0, 0])
            linear_extrude(height=3, convexity=10)
            import("PropellerDeckStand.dxf", layer="Plat");

            rotate([0, 15, 0])
            translate([0, 0, -3])
            linear_extrude(height=3, convexity=10)
            import("PropellerDeckStand.dxf", layer="Plat");
        }
        union() {
            linear_extrude(height=200, convexity=10, center=true)
            import("PropellerDeckStand.dxf", layer="PlatHole");
            linear_extrude(height=200, convexity=10, center=true)
            import("PropellerDeckStand.dxf", layer="Cut");
        }
    }
}