// Author: Olivier Lenoir <olivier.len02@gmail.com>
// Created: 2022-12-14 18:11:24
// Updated:
// License: MIT, Copyright (c) 2022 Olivier Lenoir
// Language: OpenSCAD 2021.01
// Project:
// Description:


// Special variables
$fa = 3;  // minimum angle
$fs = 0.1;  // minimum size
$fn = undef;  //number of fragments
$t = undef;  // animation step


// Viewport
$vpt = [0, 0, 10];  // translation
$vpr = [50, 0.00, 75];  // rotation angles in degrees
$vpd = 60;  // camera distance
$vpf = undef;  // camera field of view


// Modules
module Pin(head, body, height) {
    cylinder(h=head/3, d=head);
    translate([0, 0, head/3])
    cylinder(h=height, d=body);
    translate([0, 0, height+head/3])
    sphere(d=body);
}


// Main
Pin(6, 3.5, 20);
translate([0, 10, 0])
Pin(6, 3.5, 10);