// Author: Olivier Lenoir <olivier.len02@gmail.com>
// Created: 2022-11-26 12:30:27
// Updated:
// License: MIT, Copyright (c) 2022 Olivier Lenoir
// Language: OpenSCAD 2021.01
// Project: Stern Deck - Fishing Bait Boart
// Description:


// Special variables
$fa = 5;  // minimum angle
$fs = 0.1;  // minimum size
$fn = undef;  //number of fragments
$t = undef;  // animation step


// Viewport
$vpt = [0, 0, 0];  // translation
$vpr = [50, 0.00, 75];  // rotation angles in degrees
$vpd = 565;  // camera distance
$vpf = undef;  // camera field of view


// variables
// Main deck lenght 345mm
// Stern deck lenght 115mm
MainDeckLength = 345;
SternDeckLength = 115;
DeckLength = MainDeckLength + SternDeckLength;


// include
include <FishingBaitBoat.scad>


// main
if(true) {
    intersection() {
        difference() {
            SternDeckProfile(SternDeckLength);
            DeckHole(4, 20, 5, 2);
        }
        SideProfile(3, SternDeckLength, 6);
    }
}