// Author: Olivier Lenoir <olivier.len02@gmail.com>
// Created: 2022-11-05 15:32:08
// Updated:
// License: MIT, Copyright (c) 2022 Olivier Lenoir
// Language: OpenSCAD 2021.01
// Project:i Cube 20x20x15, 0.8
// Description:


// Special variables
$fa = 5;  // minimum angle
$fs = 0.1;  // minimum size
$fn = undef;  //number of fragments
$t = undef;  // animation step


// Viewport
$vpt = [45, 100, 10];  // translation
$vpr = [50, 0.00, 75];  // rotation angles in degrees
$vpd = 565;  // camera distance
$vpf = undef;  // camera field of view


// include


// use


// Main
difference() {
    cube([20, 20, 15]);
    translate([0.8, 0.8, 0.8])
        cube([20 - 1.6, 20 - 1.6, 15]);
}

// Modules


// Functions

