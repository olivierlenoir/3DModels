// Author: Olivier Lenoir <olivier.len02@gmail.com>
// Created: 2023-06-18 11:44:11
// Updated:
// License: MIT, Copyright (c) 2023 Olivier Lenoir
// Language: OpenSCAD 2021.01
// Project: Pringles Cap Box
// Description:


d=76.3;
r=0.6;
h=180;
th=3;

// include


// use


// Functions


// Modules
module torus() {
    resize([d+2*r, d+2*r, 4])
    rotate_extrude()
    translate([d/2, 0, 0])
    circle(r=r);
}


module box() {
    difference() {
        union() {
            translate([0, 0, h-2])
            torus();
            cylinder(h=h, d=d);
        }
        translate([0, 0, th])
        cylinder(d=d-2*th, h=h);
    }
};


module pipe() {
    difference() {
        union() {
            translate([0, 0, 2])
            torus();
            translate([0, 0, h-2])
            torus();
            cylinder(h=h, d=d);
        }
        cylinder(d=d-2*th, h=h);
    }
}


// Main
//box();
pipe();