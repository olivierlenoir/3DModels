// Author: Olivier Lenoir <olivier.len02@gmail.com>
// Created: 2022-11-08 21:13:46
// Updated:
// License: MIT, Copyright (c) 2022 Olivier Lenoir
// Language: OpenSCAD 2021.01
// Project: Washer, inner diameter (ID), outer diameter (OD), thickness (Th)
// Description:


module washer(ID, OD, Th) {
    linear_extrude(height=Th, slices=1)
    difference() {
        circle(d=OD);
        circle(d=ID);
    }
}


// washer(8, 16, 2);