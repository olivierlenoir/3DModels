// Author: Olivier Lenoir <olivier.len02@gmail.com>
// Created: 2022-11-08 22:00:06
// Updated:
// License: MIT, Copyright (c) 2022 Olivier Lenoir
// Language: OpenSCAD 2021.01
// Project: Thrust ball bearing
// Description:

// Special variables
//$fa = 1;  // minimum angle
//$fs = 0.1;  // minimum size
//$fn = undef;  //number of fragments
//$t = undef;  // animation step

module ring(ID, OD, BD, Th) {
    // ID: Inner diameter
    // OD: Outer diameter
    // BD: Ball diameter
    // Th: Thickness
    rotate_extrude()
    translate([(ID + OD) / 4, 0, 0])
    difference() {
        square([OD - ID, Th + BD / 3], center=true);
        translate([0, (BD / 2 + Th) - (BD / 3 + Th) / 2, 0])
        circle(d=BD);
    }
}