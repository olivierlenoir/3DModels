// Author: Olivier Lenoir <olivier.len02@gmail.com>
// Created: 2022-12-10 10:28:49
// Updated:
// License: MIT, Copyright (c) 2022 Olivier Lenoir
// Language: OpenSCAD 2021.01
// Project: Ring
// Description:


//main
rotate_extrude() {
    translate([20 / 2, 0 , 0])
    square([2, 4]);
}