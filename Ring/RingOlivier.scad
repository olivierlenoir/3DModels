// Author: Olivier Lenoir <olivier.len02@gmail.com>
// Created: 2022-12-10 14:14:57
// Updated:
// License: MIT, Copyright (c) 2022 Olivier Lenoir
// Language: OpenSCAD 2021.01
// Project: Ring Olivier
// Description:


// annulaire 20mm


module ring(diameter, tickness, height,  n) {
    difference() {
        union() {
            rotate_extrude() {
                translate([diameter / 2, 0 , 0])
                square([tickness, height]);
            }

            for(i=[0 : 360 / n : 360]) {
                rotate([0, 0, i])
                translate([diameter / 2, 0, 3])
                sphere(d= height - tickness);
            }
        }
        cylinder(h=height, d=diameter);
    }  
}


//main
ring(20, 1.2, 6, 9);