// Author: Olivier Lenoir <olivier.len02@gmail.com>
// Created: 2023-11-19 11:06:54
// Updated:
// License: MIT, Copyright (c) 2023 Olivier Lenoir
// Language: OpenSCAD 2021.01
// Project: Bird Feeder
// Description:


// Variables
holes = 6;


// Modules
module OuterBirdFeeder() {
    difference() {
        // Outer Bird Feeder
        rotate_extrude()
        import("BirdFeeder.dxf", layer="OuterBirdFeeder");
        
        // Feeder Holes
        for(r_z=[0:360/holes:180]) {
            // Feeder Holes
            rotate([90,0,r_z])
            cylinder(d=50, h=200, center=true);
        }
        // Water Holes
        for(r_z=[0:360/holes:360]) {
            rotate([0,0,r_z])
            translate([15,0,-61.5])
            cylinder(d=3, h=10, center=true);
        }
    }
}


module HoleCap() {
    translate([0,82.4621])
    intersection() {
        // Cylinder
        translate([0,-20,20])
        rotate([0,90,0])
        cylinder(r=40, h=80,center=true);

        // Hole Cap
        rotate([90,0,0])
        rotate_extrude()
        import("BirdFeeder.dxf", layer="HoleCap");
    }
}


module HolesCaps() {
    for(r_z=[0:360/holes:360]) {
        rotate([0,0,r_z])
        HoleCap();
    }
}


module Hook() {
    translate([0,0,145])
    for(r_z=[0:360/holes:180]) {
        rotate([90,0,r_z])
        rotate_extrude()
        import("BirdFeeder.dxf", layer="Hook");
    }
}


// Main
OuterBirdFeeder();
HolesCaps();
Hook();