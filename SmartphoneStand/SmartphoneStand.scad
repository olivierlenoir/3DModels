// Author: Olivier Lenoir <olivier.len02@gmail.com>
// Created: 2023-02-16 08:56:33
// Updated:
// License: MIT, Copyright (c) 2023 Olivier Lenoir
// Language: OpenSCAD 2021.01
// Project: Smartphone Stand
// Description:


linear_extrude(height=60, convexity=10)
import("SmartphoneStand.dxf", layer="stand_blackview");
