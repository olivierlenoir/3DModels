// Author: Olivier Lenoir <olivier.len02@gmail.com>
// Created: 2023-11-25 10:03:06
// Updated:
// License: MIT, Copyright (c) 2023 Olivier Lenoir
// Language: OpenSCAD 2021.01
// Project: Multi-Outlet switch cap
// Description:


// Variables
gap = 1;
thickness = 3;


// Modules
module MultiOutlet() {
    // Lumitek C74115 1667AP
    // 5 plugs with switch
    render()
    difference() {
        union() {
            for(dx=[0:212/5:212]) {
                translate([dx, 0, 0])
                intersection() {
                    sphere(d=57);
                    cube([57, 57, 40], center=true);
                }
            }
            translate([-26, 0, -7.5])
            cube([52,45,25], center=true);
            translate([-52, 0, -7.5])
            cylinder(d=45, h=25, center=true);
        }
        
        for(dx=[0:212/5:212]) {
            translate([dx, 0, 1])
            cylinder(d=39, h=19);
        }
    }
}


module SwitchCap() {
    render()
    difference() {
        union() {
            D=57+2*(thickness+gap);
            sphere(d=D);
            translate([0, 0, -10])
            cylinder(d=D, h=20, center=true);
        }
        sphere(d=57+2*gap);
        translate([0, 0, -10])
        cylinder(d=57+2*gap, h=20, center=true);
        translate([0, 0, -30])
        cube([100, 100, 20], center=true);
        translate([-26, 0, -7.5])
        cube([52,45+2*gap,25+2*gap], center=true);
        
        translate([212/5, 0, 0]) {
            cylinder(d=45, h=100, center=true);
            sphere(d=57+2*gap);
        }
        
        cylinder(d=10, h=100, center=true);
        for(az=[0:60:360]) {
            rotate([0, 0, az])
            translate([0, 15, 0])
            cylinder(d=10, h=100, center=true);
        }
    }
}


// Main
//MultiOutlet();
SwitchCap();