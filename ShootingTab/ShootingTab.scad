// Author: Olivier Lenoir <olivier.len02@gmail.com>
// Created: 2023-04-24 16:24:05
// Updated:
// License: MIT, Copyright (c) 2023 Olivier Lenoir
// Language: OpenSCAD 2021.01
// Project: Shooting tab
// Description:


// Special variables
//$fa = 5;  // minimum angle
//$fs = 0.2;  // minimum size


// include


// use


// Functions


// Modules
module Tab() {
    linear_extrude(3)
    import(file="ShootingTab.dxf", layer="tab");
}


module Sholder() {
    linear_extrude(1.2)
    import(file="ShootingTab.dxf", layer="sholder");
}


module Spacer() {
    // Index and middle finger spacer dimensions
    D_spacer = 12;
    d_spacer = 8;
    h_spacer = 24;
    // minkowski offset
    r_sphere = 2;
    d_sphere = 2 * r_sphere;

    difference() {
        minkowski() {
            difference() {
    //            linear_extrude(22)
    //            import("ShootingTab.dxf", layer="spacer");

                hull() {
                    translate([-20, -10, r_sphere])
                    cylinder(d=D_spacer-d_sphere, h=h_spacer-d_sphere);

                    translate([6, -10, r_sphere])
                    cylinder(d=d_spacer-d_sphere, h=h_spacer-d_sphere);
                }

                translate([-30+r_sphere, 0, 0])
                resize([32, 20, 22])
                rotate([90, 0, 0])
                cylinder(r=10, h=20);
            }

            sphere(r_sphere);
        }

        cube([80, 80, 6], center=true);
    }
}


module Leather() {
    color("Brown", 1.0)
    translate([0, 0, -2.3])
    linear_extrude(2.3)
    import(file="ShootingTab.dxf", layer="leather");
}



module ShootingTab() {
    union() {
        difference() {
            Tab();
    //        Sholder();
        }
        Spacer();
    }
}


// Main
//Tab();
//Spacer();
ShootingTab();
//Leather();