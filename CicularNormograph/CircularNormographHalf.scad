// Author: Olivier Lenoir <olivier.len02@gmail.com>
// Created: 2022-11-20 13:30:10
// Updated:
// License: MIT, Copyright (c) 2022 Olivier Lenoir
// Language: OpenSCAD 2021.01
// Project: Circular Normograph
// Description:


// Special variables
$fa = 5;  // minimum angle
$fs = 0.1;  // minimum size
$fn = undef;  //number of fragments
$t = undef;  // animation step


// Viewport
$vpt = [0, 0, 0];  // translation
$vpr = [50, 0.00, 75];  // rotation angles in degrees
$vpd = 500;  // camera distance
$vpf = undef;  // camera field of view


// include


// use


// Main
linear_extrude(height=3.0)
import("CircularNormograph.dxf", layer="1");


// Modules


// Functions

