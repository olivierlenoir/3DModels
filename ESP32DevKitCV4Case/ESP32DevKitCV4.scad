// Author: Olivier Lenoir <olivier.len02@gmail.com>
// Created: 2023-07-22 11:23:51
// Updated:
// License: MIT, Copyright (c) 2023 Olivier Lenoir
// Language: OpenSCAD 2021.01
// Project: ESP32 DevKit C V4
// Description:


// include


// use


// Functions


// Modules
module ESP32_DevKit_C_V4() {
    pcb_th = 1.58;
//    Board
    color("DimGray")
    cube([27.9, 48.2, pcb_th]);

//    Holes
    for(x=[1.25, 26.65]) {
        for(y=[1.24:2.54:46.96]) {
            translate([x, y, 0])
            pin();
        }
    }

//    ESP32
    translate([4.95, 28.9, pcb_th])
    ESP32_WROOM_32D();
}


module ESP32_WROOM_32D() {
//https://www.espressif.com/sites/default/files/documentation/esp32-wroom-32_datasheet_en.pdf
//    Board
    color("DarkSlateGray")
    difference() {
        cube([18, 25.5, 0.8]);

//        Side holes
        for(x=[0, 18]) {
            for(y=[1.5:1.27:18.01]) {
                translate([x, y, 0])
                cylinder(r=0.4, h=2, center=true);
            }
        }

//        Bottom holes
        for(x=[3.285:1.27:14.715]) {
            translate([x, 0, 0])
            cylinder(r=0.4, h=2, center=true);
        }
    }

//    Shield
    color("Silver")
    translate([1.1, 1.05, 0.8])
    cube([15.8, 17.6, 3.2]);
}


module pin() {
    color("Black")
    translate([0, 0, -2.54/2])
    intersection() {
        cube([2.54, 2.54, 2.54], center=true);
        rotate([0, 0, 45])
        cube([2.54, 2.54, 2.54], center=true);
    }
    color("Gold")
    translate([0, 0, -3.75])
    cube([0.64, 0.64, 11.7], center=true);
}


// Main
ESP32_DevKit_C_V4();
//ESP32_WROOM_32();
//pin();
