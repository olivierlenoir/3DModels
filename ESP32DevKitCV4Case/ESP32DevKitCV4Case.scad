// Author: Olivier Lenoir <olivier.len02@gmail.com>
// Created: 2023-07-22 11:24:19
// Updated:
// License: MIT, Copyright (c) 2023 Olivier Lenoir
// Language: OpenSCAD 2021.01
// Project: ESP32 DevKit C V4 Case
// Description:


// include


// use
use <ESP32DevKitCV4.scad>

// Functions


// Modules
module MC78T05CT() {
    difference() {
        translate([-5.14, -12.71, 0])
        union() {
            color("Silver")
            cube([10.28, 15.75, 1.39]);
            color("Black")
            cube([10.28, 9.28, 4.82]);
            translate([0, -7.135, 2.79])
            color("Silver")
            linear_extrude(0.64) {
                //Pin 1 Vin
                translate([2.58, 0, 0])
                square([0.88, 14.27], center=true);
                //Pin 2 GND
                translate([5.24, 0, 0])
                square([0.88, 14.27], center=true);
                //Pin 3 Vout
                translate([7.9, 0, 0])
                square([0.88, 14.27], center=true);
            }
        }
        cylinder(h=1.39, d=3.73);
    }
}


module MC78T05CT_holder() {
    d = 3.5;
    pos = [[0, 0], [-6.89, -10], [6.89, -10]];
    h = 4;
    linear_extrude(0.8)
    hull() {
        for(p = pos) {
            translate(p)
            circle(d=d);
        }
    }
    linear_extrude(h) {
        for(p = pos) {
            translate(p)
            circle(d=d);
        }
    }
}


module pin_holder() {
    difference() {
        square(2.8, center=true);
        square(2.8 - 1.6, center=true);
    }
}


module ESP32_DevKit_C_V4_holder() {
    // Buttom
    cube([27.9, 48.2, 0.8]);
    //    Holes
    linear_extrude(8.8)
    for(x=[1.25, 26.65]) {
        for(y=[1.24:2.54:46.96]) {
            if(
                [x, y] != [1.25, 1.24] // pin 1;1, 5V
                && [x, y] != [1.25, 13.94] // pin 1;6, GND
                && [x, y] != [26.65, 31.72] // 2; GND
                && [x, y] != [26.65, 46.96] // 2; GND
            ) {
                translate([x, y, 0])
                pin_holder();
            }
        }
    }
}


module case() {
    th = 0.8;
    translate([-(30-27.9)/2, -1.6, 0])
    difference() {
        cube([30, 60, 18]);

        translate([th, th, th])
        cube([28.4, 58.4, 18]);

        d = 1.8;
        r = d / 2;
        // Vin and GND wire holes
        for(p = [[15 - 2.66, 0, th+2.8], [15, 0, th+2.8]]) {
            translate(p)
            rotate([90, 0, 0])
            cylinder(d = d, h = 10, center=true);
        }
    }
}


module cover() {
    translate([35, -1.6, 0])
    union() {
        cube([30, 60, 0.8]);

        translate([15, 30, 0])
        linear_extrude(3)
        difference() {
            square([28, 58], center=true);
            square([26.4, 56.4], center=true);
        }
        // USB
        translate([15, 4, 0])
        cylinder(d=3, h=3.2);
        // ESP32-WROOM-32D
        translate([15, 40, 0])
        cylinder(d=3, h=2.6);
    }
}


// Main
if (false) {
    if (false) {
        translate([0, 0, 2.54 + 8.8])
        #ESP32_DevKit_C_V4();
    }
    if (false) {
        translate([13.95, 40, 0.8])
        #MC78T05CT();
    }
}


case();
translate([13.95, 40, 0])
MC78T05CT_holder();
ESP32_DevKit_C_V4_holder();
cover();
