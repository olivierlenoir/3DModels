// Author: Olivier Lenoir <olivier.len02@gmail.com>
// Created: 2022-11-12 16:00:19
// Updated:
// License: MIT, Copyright (c) 2022 Olivier Lenoir
// Language: OpenSCAD 2021.01
// Project: Turn table repair axis
// Description:


// Special variables
$fa = 3;  // minimum angle
$fs = 0.1;  // minimum size
$fn = undef;  //number of fragments
$t = undef;  // animation step


// Viewport
$vpt = [0, 0, 10];  // translation
$vpr = [50, 0.00, 75];  // rotation angles in degrees
$vpd = 100;  // camera distance
$vpf = undef;  // camera field of view


// include


// use


// Main
difference() {
    rotate_extrude()
    translate([15, 0 ,0])
    import("RepairLocker.dxf", layer="0");
    union() {
        for(i = [0 : 90 : 360]) {
            rotate(i)
            translate([10, 0, 28.5])
            cube([10, 3, 40], center=true);
        }
        translate([0, -20, 8.5])
        cube([40, 40, 40]);
    }
}

// Modules


// Functions

