// Author: Olivier Lenoir <olivier.len02@gmail.com>
// Created: 2022-11-11 16:56:27
// Updated:
// License: MIT, Copyright (c) 2022 Olivier Lenoir
// Language: OpenSCAD 2021.01
// Project: Turn table top extruded from TurnTableBase.dxf
// Description:


// Special variables
$fa = 3;  // minimum angle
$fs = 0.1;  // minimum size
$fn = undef;  //number of fragments
$t = undef;  // animation step


// Viewport
$vpt = [0, 0, 00];  // translation
$vpr = [50, 0.00, 75];  // rotation angles in degrees
$vpd = 500;  // camera distance
$vpf = undef;  // camera field of view


// include


// use


// Main
difference() {
    union() {
        linear_extrude(height=8.5)
        import("TurnTableBase.dxf", layer="0");
    }
    union() {
        rotate_extrude()
        translate([100, 11.5, 0])
        circle(d=17);
        for(i = [0, 120, 240]) {
            rotate(i)
            translate([0, 80, 4])
            cylinder(6, 0, 6);
            }
    }
}

difference() {
    rotate_extrude()
    translate([15, 0 ,0])
    import("Locker.dxf", layer="0");
    union() {
        for(i = [0 : 90 : 360]) {
            rotate(i)
            translate([10, 0, 0])
            cube([10, 3, 50], center=true);
            
        }
    }
}

// Modules


// Functions

