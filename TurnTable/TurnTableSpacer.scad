// Author: Olivier Lenoir <olivier.len02@gmail.com>
// Created: 2022-11-11 17:39:45
// Updated:
// License: MIT, Copyright (c) 2022 Olivier Lenoir
// Language: OpenSCAD 2021.01
// Project: Turn table spacer extruded from TurnTableSpacer.dxf
// Description:


// Special variables
$fa = 3;  // minimum angle
$fs = 0.1;  // minimum size
$fn = undef;  //number of fragments
$t = undef;  // animation step


// Viewport
$vpt = [0, 0, 0];  // translation
$vpr = [50, 0.00, 75];  // rotation angles in degrees
$vpd = 500;  // camera distance
$vpf = undef;  // camera field of view


// include


// use


// Main
linear_extrude(height=5)
import("TurnTableSpacer.dxf", layer="0");

// Modules


// Functions

