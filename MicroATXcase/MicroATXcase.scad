// Author: Olivier Lenoir <olivier.len02@gmail.com>
// Created: 2023-04-15 17:25:02
// Updated:
// License: MIT, Copyright (c) 2023 Olivier Lenoir
// Language: OpenSCAD 2021.01
// Project: Micro ATX case
// Description:


// Special variables
$fa = 5;  // minimum angle
$fs = 0.2;  // minimum size


// Variables

// Micro ATX mounting holes
// Motherboard: ASUS P8H61-M LE Rev. 1.03
//     uATX Form Factor
//     9.6 inch x 8.0 inch (24.4 cm x 20.3 cm)
mtg_holes = [
    [10.16, 38.1, 0],
    [76.2, 38.1, 0],
    [233.68, 38.1, 0],
    [233.68, 170.18, 0],
    [30.48, 193.04, 0],
    [76.2, 193.04, 0],
];


// include


// use


// Functions


// Modules
module Grid() {
    tickness = 3;
//    render(convexity=3)
    union() {
        for(a=[0:len(mtg_holes)-2]) {
            for(b=[a+1:len(mtg_holes)-1]) {
                hull() {
                    translate(mtg_holes[a])
                    cylinder(h=tickness, d=tickness);
                    translate(mtg_holes[b])
                    cylinder(h=tickness, d=tickness);
                }
            }
        }
    }
}



module SpacerPin() {
    union() {
        cylinder(h=10, d=10);
        cylinder(h=13, d=3.5);
        translate([0, 0, 13])
        sphere(d=3.5);
    }
}


module SpacerSrew() {
    difference() {
        cylinder(h=10, d=10);
        cylinder(h=10, d=3.5);
    }
}


module MicroATX_SpacerPin() {
//    render(convexity=3)
    union() {
        Grid();
        for(hole=mtg_holes) {
            translate(hole)
            SpacerPin();
        }
    }
}


module MicroATX_SpacerScrew() {
    union() {
        Grid();
        for(hole=mtg_holes) {
            translate(hole)
            SpacerSrew();
        }
    }
}


// Main
//Grid();
//SpacerPin();
//SpacerSrew();
MicroATX_SpacerPin();
//MicroATX_SpacerScrew();