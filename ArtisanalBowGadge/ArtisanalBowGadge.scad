// Author: Olivier Lenoir <olivier.len02@gmail.com>
// Created: 2023-04-19 19:46:28
// Updated:
// License: MIT, Copyright (c) 2023 Olivier Lenoir
// Language: OpenSCAD 2021.01
// Project: Artisanal Bow Gadge
// Description:

$fa = 2;
$fs = 0.2;

n = 5;
parabol = [for(x=[-5:0.1:5]) [x, x * x]];

linear_extrude(3)
difference() {
    circle(d=70);
    circle(d=10);
    union() {
        for(i=[1:1:n+1]) {
            rotate([0, 0, i * 360 / n])
            translate([0, 8, 0])
            polygon(parabol * i * 1.5);
        }
    }
}
