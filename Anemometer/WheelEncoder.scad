// Author: Olivier Lenoir <olivier.len02@gmail.com>
// Created: 2023-02-05 10:29:25
// Updated:
// License: MIT, Copyright (c) 2023 Olivier Lenoir
// Language: OpenSCAD 2021.01
// Project: Wheel Encoder
// Description:


// Special variables
$fa = 5;  // minimum angle
$fs = 0.1;  // minimum size
$fn = undef;  //number of fragments
$t = undef;  // animation step


// Viewport
$vpt = [0, 0, 7.5];  // translation
$vpr = [50, 0.00, 75];  // rotation angles in degrees
$vpd = 100;  // camera distance
$vpf = undef;  // camera field of view


// Main
difference() {
    union() {
        linear_extrude(height=2, convexity=10)
        import("WheelEncoder.dxf", layer="9");
        cylinder(h=15, d1=15, d2=10);
    }
    cylinder(h=40, d=5, center=true);
}