// Author: Olivier Lenoir <olivier.len02@gmail.com>
// Created: 2022-12-27 13:36:42
// Updated:
// License: MIT, Copyright (c) 2022 Olivier Lenoir
// Language: OpenSCAD 2021.01
// Project: Anemometer
// Description:


// Special variables
$fa = 3;  // minimum angle
$fs = 0.1;  // minimum size
$fn = undef;  //number of fragments
$t = undef;  // animation step


// Viewport
$vpt = [0, 0, 0];  // translation
$vpr = [50, 0.00, 75];  // rotation angles in degrees
$vpd = 600;  // camera distance
$vpf = undef;  // camera field of view


// include


// use


// Modules
module Blade(_r=25, th=1.2, _l=100) {
    translate([0, _l, 0])
    difference() {
        union() {
            translate([0, 0, _r])
            sphere(r=_r);
            translate([0, 0, _r / 10])
            rotate([90, 0, 0])
            scale([4, 1, 1])
            cylinder(d=_r / 5, h=_l);
        }
        union() {
            translate([_r,0,_r])
            cube([2 * _r, 2 * (_l + _r), 2 * _r], center=true);
            translate([0, 0, _r])
            sphere(r=_r - th);
        }
    }
};


// Main
difference() {
    union() {
        for(i=[0:120:360]) {
            rotate([0, 0, i])
            Blade();
        }
        cylinder(d1=25, d2=10, h=15);
    }
    cylinder(d=5, h=40, center=true);
}