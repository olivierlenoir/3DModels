// Author: Olivier Lenoir <olivier.len02@gmail.com>
// Created: 2022-12-31 16:54:19
// Updated:
// License: MIT, Copyright (c) 2022 Olivier Lenoir
// Language: OpenSCAD 2021.01
// Project: Attention au chien
// Description:


// Special variables
$fa = 5;  // minimum angle
$fs = 0.1;  // minimum size
$fn = undef;  //number of fragments
$t = undef;  // animation step


// Viewport
$vpt = [0, 0, 0];  // translation
$vpr = [50, 0.00, 75];  // rotation angles in degrees
$vpd = 300;  // camera distance
$vpf = undef;  // camera field of view


// Main
translate([0,0,0.6])
cube([100, 60, 1.2], center=true);
linear_extrude(3) {
    difference() {
        square([100, 60], center=true);
        square([95, 55], center=true);
    }
    
    translate([0, 12, 0])
    text("ATTENTION", size=12, font="Avant Garde", halign="center", valign="center");
    translate([0, -12, 0])
    text("AU CHIEN", size=12, font="Avant Garde", halign="center", valign="center");
}