// Author: Olivier Lenoir <olivier.len02@gmail.com>
// Created: 2022-12-31 14:15:15
// Updated:
// License: MIT, Copyright (c) 2022 Olivier Lenoir
// Language: OpenSCAD 2021.01
// Project: Stop Pub
// Description:


// Special variables
$fa = 5;  // minimum angle
$fs = 0.1;  // minimum size
$fn = undef;  //number of fragments
$t = undef;  // animation step


// Viewport
$vpt = [0, 0, 0];  // translation
$vpr = [50, 0.00, 20];  // rotation angles in degrees
$vpd = 200;  // camera distance
$vpf = undef;  // camera field of view


// Main
cylinder(h=1.2, d=60);
linear_extrude(3) {
    difference() {
        circle(d=60);
        circle(d=55);
    }
    square([40, 5], center=true);
        translate([0, 12, 0])
    text("STOP", size=12, font="Avant Garde", halign="center", valign="center");
    translate([0, -12, 0])
    text("PUB", size=12, font="Avant Garde", halign="center", valign="center");
}