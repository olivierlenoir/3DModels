// Author: Olivier Lenoir <olivier.len02@gmail.com>
// Created: 2023-09-10 12:33:57
// Updated:
// License: MIT, Copyright (c) 2023 Olivier Lenoir
// Language: OpenSCAD 2021.01
// Project: Noam's moto stand
// Description:


// Special variables
// $fa = 5;  // minimum angle
// $fs = 0.1;  // minimum size
// $fn = undef;  //number of fragments
// $t = undef;  // animation step


// Viewport
// $vpt = [45, 100, 10];  // translation
// $vpr = [50, 0.00, 75];  // rotation angles in degrees
// $vpd = 565;  // camera distance
// $vpf = undef;  // camera field of view


// include


// use


// Functions


// Modules


// Main
h=35;
difference() {
    union() {
        linear_extrude(h)
        import("NoamMotoStand.dxf", layer=1);
        linear_extrude(h + 1.2)
        import("NoamMotoStand.dxf", layer=0);
    }
    
    
    translate([0, 10, h/2])
    resize(newsize=[25, 20, 40])
    sphere(r=20);
}