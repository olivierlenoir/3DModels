// Author: Olivier Lenoir <olivier.len02@gmail.com>
// Created: 2023-06-06 18:24:57
// Updated:
// License: MIT, Copyright (c) 2023 Olivier Lenoir
// Language: OpenSCAD 2021.01
// Project: Fitting gaz
// Description:
// Link: http://joho.p.free.fr/EC/COURS%20DOC/FILETAGE/Construction/Filetages%20pour%20tuyauterie%20dits%20gaz.pdf


// Special variables
//$fa = 5;  // minimum angle
//$fs = 0.2;  // minimum size
//$fn = undef;  //number of fragments
//$t = undef;  // animation step


// Viewport
//$vpt = [45, 100, 10];  // translation
//$vpr = [50, 0.00, 75];  // rotation angles in degrees
//$vpd = 565;  // camera distance
//$vpf = undef;  // camera field of view


// include


// use


// Functions


// Modules
module cone(d1=30.291, half_angle=26.5, r=0.137329*2.309) {
    translate([r + d1 / 2, 0, 0])
    rotate([0, 90, 0])
    minkowski() {
        cylinder(h=5, r1=0, r2=5*tan(half_angle));
        sphere(r);
    }
}


module thread(pitch=2.309, h=28) {
    step = $fa;
    for(z=[-360:step:360*h/pitch+360]) {
        hull() {
            translate([0, 0, z*pitch/360])
            rotate([0, 0, z])
            children();
            
            z_a = z + step;
            translate([0, 0, z_a*pitch/360])
            rotate([0, 0, z_a])
            children();
        }
    }
}


// Main
intersection() {
    thread()
    cone();
    cylinder(d=41, h=28);
}
translate([0, 0, 28])
difference() {
    cylinder(d=41, h=28);
    cylinder(d=35, h=28);
    translate([0, 0, 25])
    cylinder(h=3, d1=35, d2=38);
}