// Author: Olivier Lenoir <olivier.len02@gmail.com>
// Created: 2022-11-01 16:14:31
// Updated:
// License: MIT, Copyright (c) 2022 Olivier Lenoir
// Language: OpenSCAD 2021.01
// Project: Cube Hole
// Description: 3D printer measure calibration


// Special variables
$fa = 5;  // minimum angle
$fs = 0.1;  // minimum size
$fn = undef;  //number of fragments
$t = undef;  // animation step


// Viewport
$vpt = [45, 100, 10];  // translation
$vpr = [50, 0.00, 75];  // rotation angles in degrees
$vpd = 565;  // camera distance
$vpf = undef;  // camera field of view


// include


// use


// Main
difference() {
    union() {
        difference() {
            cube([50, 30, 20]);
            translate([3, 3, 3])
                cube([44, 24, 20]);
        }
        translate([8, 8, 10])
            cylinder(h=20, r=8, center=true);
        translate([37, 15, 10])
            cylinder(h=20, r=13, center=true);
    }
    union() {
        translate([8, 8, 10])
            cylinder(h=55, r=5, center=true);
        translate([37, 15, 10])
            cylinder(h=55, r=10, center=true);
    }
}


// Modules


// Functions

