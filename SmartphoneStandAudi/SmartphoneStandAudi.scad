// Author: Olivier Lenoir <olivier.len02@gmail.com>
// Created:
// Updated:
// License: MIT, Copyright (c) 2023 Olivier Lenoir
// Language: OpenSCAD 2021.01
// Project: Smartphone Stand Audi
// Description:


// include


// use


// Functions


// Modules
module audi_cup() {
    rotate_extrude()
    import("SmartphoneStandAudi.dxf", layer="0");
}


module stand_holder() {
    intersection() {
        rotate_extrude()
        import("SmartphoneStandAudi.dxf", layer="outer");
        
        difference() {
            rotate([90, 0, 0])
            linear_extrude(center=true)
            import("SmartphoneStandAudi.dxf", layer="stand_holder");
            
            translate([0, 0, 82/2 - 15])
            rotate([0, 90, 0])
            cylinder(h=82, d=82, center=true);
        }
    }
}

// Main
audi_cup();
stand_holder();

rotate([0, 0, 180])
stand_holder();